#FROM middlefork/basegis
FROM middlefork/basegis

RUN apt update \
  && apt install -y \
  cgi-mapserver \
  libmapserver2 \
  libmapserver-dev \
  mapserver-bin \
  postgresql-client-common \
  python-mapscript \
  python-pip \
  && pip install uwsgi \
  && mkdir /mapserver

RUN useradd -ms /bin/bash mapserver

USER mapserver
WORKDIR /mapserver

COPY uwsgi.ini /mapserver-uwsgi.ini
COPY mapserver_app.py /mapserver
COPY data /mapserver/data/
COPY maps /mapserver/maps/
COPY etc /mapserver/etc/

EXPOSE 8080
#CMD uwsgi --emperor /mapserver
CMD uwsgi --ini /mapserver-uwsgi.ini



