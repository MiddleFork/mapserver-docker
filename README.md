#Mapserver WMS

## OSGEO mapserver served with uwsgi

This provides an instance of a mapserver WMS server running under python mapscript.OWSRequest() and served via uwsgi.

As such, the cgi-mapserver is not used or required.

A sample map and data are provided for testing with your local WMS client.

## Dockerizing Mapserver 

Run mapserver using OWSRequest() instead of mapserver-cgi.   

Serve mapserver WMS via uwsgi, exposing it on port 8080

No more mod_apache!  No more apache at all!

Follows 'best practices' in that the container only runs a single process - the uwsgi app with OWSRequest()


## Dockerizing Map Files

A dockerized mapserver mapfile must allow other clients on the Docker IP space to access this service.

If this were done manually, each legacy mapfile would need to be manually modified (edited)to include the following content:

```
MAP
  ...
  WEB
    ...
    METADATA
      ...
      "ows_allowed_ip_list" "172.16.0.0/12" 
    END
  END
END
```

Without this modification, other services would not be able to access the mapserver wms service.

These manual modifications would be cumbersome, and prone to error or forgetfulness.

By virtue of mapscript we are able to modify our mapfiles programatically as seen in this snippet:

```
map = mapscript.mapObj('/path/to/my/mapfile.map')
map.web.metadata['ows_allowed_ip_list'] = '172.16.0.0/12'
```
