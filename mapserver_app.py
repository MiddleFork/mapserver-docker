import os
import sys
import mapscript


def application(environ, start_response):
    """WSGI application for mapserver OWS WMS Server"""
    try:
        req = mapscript.OWSRequest()
        req.loadParamsFromURL(environ.get("QUERY_STRING"))
        req.setParameter('SRS', 'EPSG:4326')

        the_mapfile_name = req.getValueByName('MAP')
        the_mapfile_path = os.path.join(os.getcwd(), 'maps')

        if the_mapfile_name.startswith(the_mapfile_path):
            the_mapfile = the_mapfile_name

        else:
            the_mapfile = os.path.join(the_mapfile_path, the_mapfile_name)

        if os.path.exists(the_mapfile):
            try:
                'Building mapObj from ', os.path.basename(the_mapfile)
                map = mapscript.mapObj(the_mapfile)

            except:
                raise Exception
        else:
            raise Exception

        #Critical:
           # Allow all clients in the private docker IP space (172.16.0.0/12) to access this service
           # Otherwise clients (such as mapproxy, openlayers, etc. will fail with permission denied errors
        map.web.metadata['ows_allowed_ip_list'] = '172.16.0.0/12'

        mapscript.msIO_installStdoutToBuffer()
        map.OWSDispatch(req)
        content_type = mapscript.msIO_stripStdoutBufferContentType()
        content = mapscript.msIO_getStdoutBufferBytes()

        if content_type == "vnd.ogc.se_xml":
            content_type = "text/xml"

        start_response("200 Ok", [("Content-type", content_type)])
        return [content]

    except:
        start_response("500 Server Error: mapserver_app.py", [("Content-type", "text/plain")])
        return mapscript.msIO_getStdoutBufferString()
