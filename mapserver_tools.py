

def allow_docker_hosts(map_obj):
    map_obj.web.metadata['ows_allowed_ip_list'] = '172.16.0.0/12'
    return map_obj

